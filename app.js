<!DOCTYPE html>
<html>
<head>
    <title>Loyalty Token App</title>
    <script src="https://cdn.jsdelivr.net/npm/web3@1.6.0/dist/web3.min.js"></script>
</head>
<body>
    <h1>Loyalty Token App</h1>
    <p>Account: <span id="account"></span></p>
    <p>Balance: <span id="balance"></span> LOYAL</p>

    <label for="allocateAmount">Allocate Tokens:</label>
    <input type="number" id="allocateAmount" placeholder="Amount">
    <button onclick="allocateTokens()">Allocate</button>

    <hr>

    <label for="purchaseAmount">Make a Purchase:</label>
    <input type="number" id="purchaseAmount" placeholder="Amount">
    <button onclick="makePurchase()">Purchase</button>

    <hr>

    <label for="redeemAmount">Redeem Tokens:</label>
    <input type="number" id="redeemAmount" placeholder="Amount">
    <button onclick="redeemTokens()">Redeem</button>

    <script>
        // Replace with the contract address and ABI
        const contractAddress = 'YOUR_CONTRACT_ADDRESS';
        const contractABI = [
            // Paste the ABI here
        ];

        let web3;
        let contractInstance;

        async function initialize() {
            if (typeof window.ethereum !== 'undefined') {
                web3 = new Web3(window.ethereum);
                try {
                    await window.ethereum.enable();
                } catch (error) {
                    console.error("User denied account access");
                }
            } else if (typeof window.web3 !== 'undefined') {
                web3 = new Web3(window.web3.currentProvider);
            } else {
                console.error("No web3 provider detected");
            }

            contractInstance = new web3.eth.Contract(contractABI, contractAddress);
            updateAccountInfo();
        }

        async function updateAccountInfo() {
            const accounts = await web3.eth.getAccounts();
            if (accounts.length > 0) {
                const account = accounts[0];
                document.getElementById("account").textContent = account;
                const balance = await contractInstance.methods.balanceOf(account).call();
                document.getElementById("balance").textContent = web3.utils.fromWei(balance, 'ether');
            } else {
                document.getElementById("account").textContent = "Not connected";
                document.getElementById("balance").textContent = "N/A";
            }
        }

        async function allocateTokens() {
            const allocateAmount = document.getElementById("allocateAmount").value;
            if (allocateAmount <= 0) return;
            
            const accounts = await web3.eth.getAccounts();
            if (accounts.length === 0) {
                console.error("Not connected to a wallet");
                return;
            }

            const account = accounts[0];
            try {
                await contractInstance.methods.allocateTokens(account, web3.utils.toWei(allocateAmount, 'ether')).send({ from: account });
                updateAccountInfo();
            } catch (error) {
                console.error(error);
            }
        }

        async function makePurchase() {
            const purchaseAmount = document.getElementById("purchaseAmount").value;
            if (purchaseAmount <= 0) return;

            const accounts = await web3.eth.getAccounts();
            if (accounts.length === 0) {
                console.error("Not connected to a wallet");
                return;
            }

            const account = accounts[0];
            try {
                await contractInstance.methods.makePurchase(web3.utils.toWei(purchaseAmount, 'ether')).send({ from: account });
                updateAccountInfo();
            } catch (error) {
                console.error(error);
            }
        }

        async function redeemTokens() {
            const redeemAmount = document.getElementById("redeemAmount").value;
            if (redeemAmount <= 0) return;

            const accounts = await web3.eth.getAccounts();
            if (accounts.length === 0) {
                console.error("Not connected to a wallet");
                return;
            }

            const account = accounts[0];
            try {
                await contractInstance.methods.redeemTokens(web3.utils.toWei(redeemAmount, 'ether')).send({ from: account });
                updateAccountInfo();
            } catch (error) {
                console.error(error);
            }
        }

        window.addEventListener('load', initialize);
    </script>
</body>
</html>
