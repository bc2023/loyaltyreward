// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

// Import the SafeMath library to handle safe arithmetic operations
import "@openzeppelin/contracts/utils/math/SafeMath.sol";

// Import the Ownable contract to manage contract ownership
import "@openzeppelin/contracts/access/Ownable.sol";

// Define the loyalty token contract
contract LoyaltyToken is Ownable {
    using SafeMath for uint256;

    // Token details
    string public name = "Loyalty Token";
    string public symbol = "LOYAL";
    uint8 public decimals = 18;
    uint256 public totalSupply;

    // Mapping to track user balances
    mapping(address => uint256) public balanceOf;

    // Mapping to track user purchases
    mapping(address => uint256) public purchaseAmount;

    // Event to log token transfers
    event Transfer(address indexed from, address indexed to, uint256 value);

    // Event to log token allocation
    event TokensAllocated(address indexed user, uint256 amount);

    // Constructor to initialize the contract
    constructor(uint256 initialSupply) {
        totalSupply = initialSupply * 10 ** uint256(decimals);
        balanceOf[msg.sender] = totalSupply;
    }

    // Function to allocate tokens to a user
    function allocateTokens(address user, uint256 amount) external onlyOwner {
        require(user != address(0), "Invalid user address");
        require(amount > 0, "Amount must be greater than zero");
        require(totalSupply >= amount, "Not enough tokens in the contract");

        totalSupply = totalSupply.sub(amount);
        balanceOf[user] = balanceOf[user].add(amount);
        emit TokensAllocated(user, amount);
    }

    // Function to allow users to make purchases and earn tokens
    function makePurchase(uint256 amount) external {
        require(amount > 0, "Amount must be greater than zero");
        require(balanceOf[msg.sender] >= amount, "Insufficient balance");

        purchaseAmount[msg.sender] = purchaseAmount[msg.sender].add(amount);
        balanceOf[msg.sender] = balanceOf[msg.sender].add(amount);
        emit Transfer(address(0), msg.sender, amount);
    }

    // Function to redeem tokens for rewards or discounts
    function redeemTokens(uint256 amount) external {
        require(amount > 0, "Amount must be greater than zero");
        require(balanceOf[msg.sender] >= amount, "Insufficient balance");

        balanceOf[msg.sender] = balanceOf[msg.sender].sub(amount);
        totalSupply = totalSupply.add(amount);
        emit Transfer(msg.sender, address(0), amount);
    }
}
